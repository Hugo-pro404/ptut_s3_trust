\documentclass[class=article, crop=false]{standalone}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{lastpage}

\begin{document}

\section{Chiffrement RSA}

\textit{Cette partie est grandement inspirée du dossier de modélisations mathématiques que certains d'entre nous ont réalisé au 3ème semestre.}

\subsection{Introduction}

Le chiffrement RSA est un algorithme de cryptographie asymétrique présenté en 1977. Il permet de chiffrer et déchiffrer un message (un nombre entier) grâce à une paire de clés : une clé publique et une clé privée. \\
Inventé par Ronald Rivest, Adi Shamir et Leonard Adleman (d’où  RSA), il se décompose en 2 phases : 
une phase de génération des clés et une phase de chiffrement. Pour terminer, nous décrirons la partie classique de l'algorithme de Shor, un algorithme de factorisation dont une partie repose sur l'informatique quantique.

\subsection{Cryptographie asymétrique}

Par opposition à la cryptographie symétrique qui utilise une clé unique pour le chiffrement et déchiffrement de données, le fonctionnement de la cryptographie asymétrique repose sur une paire de clés. Les données chiffrées par une clé ne peuvent être déchiffrées que par l'autre clé de la paire. Quand Bob souhaite envoyer un message à Alice, il doit disposer de sa clé publique. Alice étant le seul utilisateur qui dispose de la clé privé, personne d'autre qu'elle ne peut déchiffrer le message. \\

\begin{figure}[htp]
    \centering
    \includegraphics[scale=0.35]{../ressources/chAsym.jpg}
\end{figure}

La cryptographie asymétrique peut être utilisée pour chiffrer des données (même si elle est plus lente que les algorithmes de chiffrement symétriques qui n'utilisent qu'une clé pour chiffrement et déchiffrement) mais aussi pour signer des données à envoyer. Pour cela, l'algorithme de chiffrement asymétrique est associé à une fonction de hachage (SHA-256 par exemple).



\subsection{Fonctionnement général}

\subsubsection{Génération des clés}
\footnote{\url{https://www.johndcook.com/blog/2018/12/12/rsa-exponent/}}
Voici les étapes de la génération des clés.
\begin{itemize}
  \item On choisit 2 (grands) nombres premiers \(p, q \in \mathbb{N}^*\), \(p \neq q\) 
  \item On définit \(n \in \mathbb{N}^*\) ainsi : \(n = p \times q\) et on calcule la valeur en \(n\) de l'indicatrice d'Euler (nombre d'éléments entre 1 et n premiers avec n) \(\varphi(n) = (p - 1) \times (q - 1)\) puisque \(n = p \times q\) et que \(p\) et \(q\) sont premiers.
  \item On prend maintenant un élément \([e] \in (\mathbb{Z} / \varphi(n)\mathbb{Z})^{\times}\) (inversible modulo \(\varphi(n)\)) et on cherche \([d] = [e]^{-1}\) qui est unique.
  
\end{itemize}

On obtient ainsi les couples \((e, n)\) et \((d, n)\) qui correspondent respectivement à la clé publique et la clé privée. En théorie, les couples peuvent être échangés et il est tout à fait possible d'utiliser \((d, n)\) comme clé publique et \((e, n)\) comme clé privée. Cependant, beaucoup d'implémentations de RSA choisissent \(e = 65537\) puis génèrent des valeurs pour \(p\) et \(q\) jusqu'à ce que \(\varphi(n) = (p - 1) \times (q - 1)\) soit premier avec \(e\). \\

Ce choix pour \(e\) est dû au fait qu'élever une valeur à l'exposant \(65537\) consiste pour la majorité des étapes à élever des valeurs au carré (car \(2^{16} + 1 = 2^{2^4} + 1 = 65537\) qui est le plus grand nombre de Fermat premier connu, les autres étant \(3\), \(5\), \(17\), \(257\) de la forme \(2^{2^n} + 1\) avec \(n \in \mathbb{N}^*\)). \\

Bien évidemment, si l'on fixe \(e = 65537\) (ce qui est fait dans la majorité des implémentations), la clé \((e, n)\) doit alors être la clé publique (sinon l'exposant privé sera connu puisque égal à 65537).

\subsubsection{Chiffrement et déchiffrement}

L'étape de génération des clés est la plus difficile en RSA. Une fois les clés obtenues, les processus de chiffrement et de déchiffrement sont simples à mettre en oeuvre. Supposons que nous avons les clés \((e, n)\) et \((d, n)\) avec \((e, n)\) la clé publique et \((d, n)\) la clé privée.

\paragraph{Chiffrement}
RSA peut chiffrer des entiers naturels inférieurs à \(n = p \times q\).
Le message à chiffrer est noté \(m \in \mathbb{N}^*, m < n\), qui devient un représentant d'une classe dans \(\mathbb{Z}/n\mathbb{Z}\) (la classe \([m]\)).\\
le message chiffré, noté \(c\) (qui sera nécessairement inférieur à \(n\)) est calculé comme suit :
\[c \equiv m^e \mod n\]

\paragraph{Déchiffrement}
Cette étape utilise \(d\), inverse multiplicatif de \(e\) modulo \(\varphi(n)\). On a la relation suivante pour retrouver le message initial :
\[m \equiv c^d \text{ mod } n\]

\paragraph{Justification}
\footnote{\url{http://therese.eveilleau.pagesperso-orange.fr/pages/truc_mat/textes/RSA.htm}}

Puisque nous avons \(c \equiv m^e \text{ mod } n\), le déchiffrement donne :
\[c^d = (m^e)^d = m^{e \times d}\]
Or, \(e \times d \equiv 1 \text{ mod } \varphi(n)\) donc \(e \times d = k\varphi(n) + 1\) avec \(k \in \mathbb{N}\).\\
De ce fait :
\[m^{e \times d} = m^{k\varphi(n) + 1} = m^{k(p - 1)(q - 1) + 1}\]
\begin{itemize}
    \item Si \(m\) n'est divisible ni par \(p\) ni par \(q\), d'après le petit théorème de Fermat, on a \(m^{p - 1} \equiv 1 \text{ mod } p\) et \(m^{q - 1} \equiv 1 \text{ mod } q\).
    \[m^{ed} \equiv m^{k(p - 1)(q - 1) + 1} \equiv (m^{p - 1})^{k(q - 1)} \times m \equiv 1^{k(q - 1)} \times m \equiv m \text{ mod } p\]
    De la même manière :
    \[m^{ed} \equiv m^{k(p - 1)(q - 1) + 1} \equiv (m^{q - 1})^{k(p - 1)} \times m \equiv 1^{k(p - 1)} \times m \equiv m \text{ mod } q\]
    
    \item Si \(p\) divise \(m\) et \(q\) ne divise pas \(m\), alors \(m \equiv 0 \text{ mod } p\) et \(m^{ed} \equiv 0 \text{ mod } p\) donc \(m^{ed} \equiv m \text{ mod } p\). \\
    
    De plus, d'après le petit théorème de Fermat, \(m^{(q - 1)} \equiv 1 \text{ mod } q\) donc \(m^{ed} \equiv m^{k\varphi(n) + 1} \equiv m^{k(p - 1)(q - 1)} \times m \equiv 1^{k(p - 1)} \times m \equiv m \text{ mod } q\). Soit : \(m^{ed} \equiv m \text{ mod } q\). \\
    
    \item Si \(p\) et \(q\) divisent \(m\), on a également \(m \equiv 0 \mod p\) et \(m \equiv 0 \mod q\) donc \(m^{ed} \equiv 0 \mod p\) et \(m^{ed} \equiv 0 \mod q\) et donc \(m^{ed} \equiv m \mod p\) et \(m^{ed} \equiv m \mod q\). \\
    Cette situation n'est possible que si \(m = 0\). En effet, supposons que \(p | m\) et \(q | m\) avec \(0 < m < n\). \(p | m\) donc il existe \(a \in \mathbb{N}^*\) tel que \(pa = m\). De plus \(q | m\) donc \(q | pa\) et \(q\) ne divise pas \(p\) donc \(q | a\) et il existe \(b \in \mathbb{N}^*\) tel que \(qb = a\). \(pa = m\) et \(qb = a\) donc \(m = pqb\). Comme \(b > 0\), on en déduit que \(m \geq n\) ce qui est absurde donc \(p\) et \(q\) ne peuvent pas diviser \(m\) en même temps quand \(m \neq 0\). \\
    
\end{itemize}

Dans ces trois situations, on a \(m^{ed} \equiv m \mod p\) et \(m^{ed} \equiv m \mod q\) donc \(m^{ed} - m \equiv 0 \mod p\) et \(m^{ed} - m \equiv 0 \mod q\).

Donc il existe \(a, b \in \mathbb{Z}\) tel que \(m^{ed} - m = ap\) et \(m^{ed} - m = bq\).
    \[m^{ed} - m = ap = bq\]
    Donc \(p|bq\) et comme \(q\) est premier différent de \(p\), alors \(p|b\). Il existe \(c \in \mathbb{Z}\) tel quel \(b = cp\). On peut alors écrire :
    
    \[m^{ed} - m = ap = cpq = cn\]
    \[m^{ed} - m \equiv 0 \mod n\]
    \[m^{ed} \equiv m \mod n\]
    
    
\subsection{Exemple de chiffrement RSA avec \(p\) et \(q\) petits}

Prenons un exemple simple de génération de clés, de chiffrement et de déchiffrement. Soient \(p = 7\) et \(q = 11\).
De ce fait, \(n = p \times q = 7 \times 11 = 77\). On peut maintenant calculer \(\varphi(n) = \varphi(77) = (p - 1) \times (q - 1) = 6 \times 10 = 60\). \newline
Choisissons \(e = 43\) (PGCD(43, 60) = 1). Nous cherchons maintenant \(d\), l'inverse multiplicatif de \(e\) modulo \(\varphi(n) = 60\). Cet inverse existe puisque \(e\) et \(\varphi(n)\) sont premiers entre eux.

\[60 = 43 + 17\]
\[43 = 2 \times 17 + 9\]
\[17 = 1 \times 9 + 8\]
\[9 = 1 \times 8 + 1\]

On note, suite à l'exécution de l'algorithme d'Euclide, que \(60\) et \(43\) sont bien premiers entre eux.

\[1 = 9 - 8\]
\[1 = 9 - (17 - 9)\]
\[1 = 9 - 17 + 9\]
\[1 = 2 \times 9 - 17\]
\[1 = 2(43 - 2 \times 17) - 17\]
\[1 = 2 \times 43 - 4 \times 17 - 17\]
\[1 = 2 \times 43 - 5 \times 17\]
\[1 = 2 \times 43 - 5(60 - 43)\]
\[1 = 2 \times 43 - 5 \times 60 + 5 \times 43\]
\[1 = 7 \times 43 - 5 \times 60\]
\[7 \times 43 = 5 \times 60 + 1\]
On a donc \(7\), l'inverse de \(43\) modulo \(60\).

donc \((e, n)\) = \((43, 77)\) et \((d, n)\) = \((7, 77)\) sont les clés recherchées. \\
Supposons maintenant que nous voulons chiffrer la valeur \(m = 15\). \\
\[c \equiv m^e \mod n\]
Donc
\[15^{43} \mod 77\]
\[= 15 \times 15 \times 15^{41} \mod 77\]
\[= 225 \times 15^{41} \mod 77\]
\[= (77 \times 2 + 71) \times 15^{41} \mod 77\]
\[= 71 \times 15^{41} \mod 77\]
\[= 71 \times 15 \times 15^{40} \mod 77\]
\[= 1065 \times 15^{40} \mod 77\]
\[= (13 \times 77 + 64) \times 15^{40} \mod 77\]
\[= 64 \times 15^{40} \mod 77\]
\[= 64 \times 15 \times 15^{39} \mod 77\]
\[= 960 \times 15^{39} \mod 77\]
\[= (12 \times 77 + 36) \times 15^{39} \mod 77\]
\[= 36 \times 15^{39} \mod 77\]
\[= 36 \times 15 \times 15^{38} \mod 77\]
\[= 540 \times 15^{38} \mod 77\]
\[= (7 \times 77 + 1) \times 15^{38} \mod 77\]
\[= 1 \times 15^{38} \mod 77\]
On remarque donc que \(15^5 \equiv 1 \mod 77\).
\[15^{38} \mod 77\]
\[= 15^{35} \times 15^3 \mod 77\]
\[= (15^5)^7 \times 15^3 \mod 77\]
\[= 15^3 \mod 77\]
\(15^3 \mod 77\) a déjà été calculé donc :
\[c \equiv 15^3 \equiv 64 \mod 77\]

Nous pouvons maintenant déchiffrer le message :

\[c^d \mod 77 = 64^7 \mod 77\]
\[= 64 \times 64 \times 64^5 \mod 77\]
\[= (53 \times 77 + 15) \times 64^5 \mod 77\]
\[= 15 \times 64^5 \mod 77\]
Puisque \(64^2 \equiv 15 \mod 77\), on a :
\[= 15 \times 64^5 \mod 77\]
\[= 15 \times 64 \times 64^2 \times 64^2 \mod 77\]
\[= 960 \times 15 \times 15 \mod 77\]
\[= (12 \times 77 + 36) \times 15 \times 15 \mod 77\]
\[= 36 \times 225 \mod 77\]
\[= 36 \times (2 \times 77 + 71) \mod 77\]
\[= 36 \times 71 \mod 77\]
\[= 2556 \mod 77\]
\[= 77 \times 33 + 15 \mod 77\]
\[= 15 \mod 77\]

\[c^d \equiv 15 \equiv m \mod 77\]

On retrouve bien le message de départ.

\subsection{Factorisation de clés RSA avec l'algorithme de Shor}

Nous avions décrit cet algorithme lors du projet de modélisations et il nous a semblé intéressant, puisque qu'il aurait un impact significatif sur RSA s'il pouvait être implémenté en pratique, de reprendre cette présentation.
\footnote{\url{https://cel.archives-ouvertes.fr/cel-00963668/document}}

L'algorithme de Shor permet de factoriser \(n = pq\) bien plus rapidement que les algorithmes classique que l'on connaît. S'il pouvait être implémenté en pratique, la méthode chiffrement RSA deviendrait vulnérable car trop rapide à casser. Cet algorithme se décompose en deux parties : une partie classique et une partie quantique de recherche de période (c'est cette partie qui explique son efficacité). \\
 Dans ce dossier, nous nous limiterons à décrire le fonctionnement de la partie classique de cet algorithme.

\subsubsection{Partie classique}

\begin{itemize}
    \item prendre un nombre pseudo-aléatoire \(a \in \mathbb{N}^*, 1 < a < n\) puis calculer \(PGCD(a, n)\). \\
    
    Si ce PGCD est différent de 1, alors c'est un facteur non trivial de \(n\) (différent de \(1\) et de \(n\)) puisque \(a < n\). On a trouvé la solution.\\

    Sinon, on recherche la période \(r\) de la fonction \(f : x \longmapsto a^x \mod n\). C'est-à-dire le plus petit entier \(r\) tel que pour tout \(x\), \(f(x + r) = f(x)\). Donc \(a^{x + r} \equiv a^x \mod n\) pour tout \(x\), donc pour \(x = 0\), on a \(a^r \equiv 1 \mod n\). (De plus, \(a^r \equiv 1 \mod n \implies a^x \times a^r \equiv a^x \mod n\) pour tout \(x\)). Nous recherchons donc l'ordre multiplicatif de \([a]\) dans le groupe \(((\mathbb{Z} / n\mathbb{Z})^\times, \times)\) \\
    

Puisque \(PGCD(a, n) = 1\), d'après le théorème d'Euler, \(a^{\varphi(n)} \equiv 1 \mod n\) donc il existe \(r \in \mathbb{N}^*\) avec \(r \leq \varphi(n)\) tel que :

\[a^r \equiv 1 \mod n\]

La recherche s'effectue avec la partie quantique de l'algorithme de Shor. \\
Une fois la période trouvée : \\

    \item Si \(r\) est impair recommencer. \\
    
    Sinon : \\
    On a \(a^r \equiv 1 \mod n\) donc il existe \(k \in \mathbb{Z}\) tel que \(a^r - 1 = kn\). Puisque \(r\) est pair, ceci peut s'écrire \((a^{r / 2} - 1) \times (a^{r / 2} + 1) = kn\). \\

    On note que \(n\) ne peut pas diviser \(a^{r / 2} - 1\). Supposons que cela soit possible, alors \(a^{r / 2} - 1 \equiv 0 \mod n\) donc \(a^{r / 2} \equiv 1 \mod n\) donc la période \(r\) trouvée n'est pas correcte puisque \(r / 2 < r\) (car \(r\) est un entier strictement positif) et vérifie la propriété de la période. Ceci est absurde, donc \(n\) ne divise pas \(a^{r / 2} - 1\). \\

    \item Si \(a^{r / 2} \equiv -1 \mod n\) donc \(a^{r / 2} + 1 \equiv 0 \mod n\) ce qui signifie que \(n | (a^{r/2} + 1)\) donc \(PGCD(n, a^{r / 2} + 1) = n\) c'est une solution triviale : il faut recommencer. \\

    \item Sinon, puisque \(n | (a^{r / 2} - 1) \times (a^{r / 2} + 1)\) mais que \(n\) ne divise ni \(a^{r / 2} - 1\) (car impossible par définition de \(r\)), ni \(a^{r/2} + 1\) (car aurait été traité au cas précédent), alors \(n\) partage des facteurs non triviaux avec \(a^{r / 2} + 1\) et \(a^{r / 2} - 1\). Les solutions recherchées sont bien \(PGCD(a^{r / 2} - 1, n)\) et \(PGCD(a^{r / 2} + 1, n)\).

\

\end{itemize}

\subsubsection{Partie quantique}

L'objectif de cette partie de l'algorithme est de trouver la période de la fonction \(f : x \mapsto a^x \mod n\). Il n'existe aucun algorithme classique pour effectuer cela efficacement. C'est en ce point que la partie quantique est intéressante et que l'algorithme de Shor devient très efficace.


\end{document}
