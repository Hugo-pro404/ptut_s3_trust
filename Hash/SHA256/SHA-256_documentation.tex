\documentclass[class=article, crop=false]{standalone}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{hyperref}

\begin{document}

\section{SHA-256}
\subsection{Introduction}

Cette partie est une description du fonctionnement de l'algorithme de hachage SHA-256 publiée en 2002 par le NIST (National Institute of Standards and Technology). Le résultat de cet algorithme est un haché de 256 bits, quelle que soit la taille des données fournies en entrée. Il est plus résistant aux collisions que SHA-1 (dévalorisé en 2011) qui produit des hachés de 160 bits (Google est parvenu à générer une collision avec 2 fichiers PDF avec SHA-1).

Nous nous consacrons ici à SHA-256. Un exemple complet de hachage avec cette fonction est présenté en annexe.

\subsection{Fonctionnement}
Avant le traitement principal, SHA-256 prépare les données à traiter; c'est l'étape de bourrage.

\subsubsection{Bourrage}

Cette étape est identique à l'étape du même nom dans SHA-1.

\subsubsection{Traitement principal}

Cette phase utilise principalement ces 6 fonctions :

\[Ch(x, y, z) = (x \land y) \oplus (\neg x \land z)\]
\[Maj(x, y, z) = (x \land y) \oplus (x \land z) \oplus (y \land z)\]
\[\Sigma_0(x) = ror(x, 2) \oplus ror(x, 13) \oplus ror(x, 22)\]
\[\Sigma_1(x) = ror(x, 6) \oplus ror(x, 11) \oplus ror(x, 25)\]
\[\sigma_0(x) = ror(x, 7) \oplus ror(x, 18) \oplus (x >> 3)\]
\[\sigma_1(x) = ror(x, 17) \oplus ror(x, 19) \oplus (x >> 10)\]

Où \(ror(x, n)\) représente une rotation des bits de x vers la droite de n bits et \(x >> n\) un décalage de n bits vers la droite (\(x\) est un mot de 32 bits). \\

L’algorithme utilise aussi 64 constantes : il s’agit des 32 premiers bits de la partie décimale de la racine cubique des 64 premiers nombres premiers. On obtient alors les constantes suivantes :

\[0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5\]
\[0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5\]
\[0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3\]
\[0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174\]
\[0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc\]
\[0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da\]
\[0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7\]
\[0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967\]
\[0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13\]
\[0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85\]
\[0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3\]
\[0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070\]
\[0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5\]
\[0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3\]
\[0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208\]
\[0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2\] \newline

On définit ici les notations utilisées tout au long de cette documentation : \newline

\noindent\(a, b, c, d, e, f, g\) et \(h\) : 8 registres de travail (mots de 32 bits). \newline
\(hash[i]\) : \(i^{eme}\) mot de 32 bits de hachage intermédiaire. \newline
\(M[i]\) : \(i^{eme}\) bloc du résultat du bourrage. \newline
\(N\) le nombre de blocs. \newline
T1 et T2 : mots de 32 bits. \newline
\(K\) : tableau contenant les constantes définies ci-dessus. \newline
\(W\) : tableau de 64 mots de 32 bits.

\paragraph{Initialisation :}
Les variables de hachage sont initialisées avec les valeurs suivantes : 
\[hash[0] = 0x6a09e667\]
\[hash[1] = 0xbb67ae85\]
\[hash[2] = 0x3c6ef372\]
\[hash[3] = 0xa54ff53a\]
\[hash[4] = 0x510e527f\]
\[hash[5] = 0x9b05688c\]
\[hash[6] = 0x1f83d9ab\]
\[hash[7] = 0x5be0cd19\] \newline

Ces premières valeurs sont obtenues en prenant les 32 premiers bits de la partie décimale de la racine carrée des 8 premiers nombres premiers (le détail du calcul est présenté en annexe).

\paragraph{Hachage : }
Pour chaque bloc \(B = M[i]\) avec \(0 \leq i \leq N - 1\) de 512 bits (découpé en éléments de 32 bits), on note \(B[i]\) le \(i^{eme}\) mot de 32 bits de \(M[i]\).
\newline

Initialisation du tableau \(W\)
\[
W[t] = 
\begin{cases} 
    B[t] & 0 \leq t \leq 15\\
    \sigma_1(W[t - 2]) + W[t - 7] + \sigma_0(W[t - 15]) + W[t - 16] & 16 \leq t \leq 63
\end{cases}
\]


On initialise ensuite les variables de travail avec la valeur de hachage, soit : 

\[a = hash[0]\]
\[b = hash[1]\]
\[c = hash[2]\]
\[d = hash[3]\]
\[e = hash[4]\]
\[f = hash[5]\]
\[g = hash[6]\]
\[h = hash[7]\] \newline

On affecte maintenant de nouvelles valeurs aux registres de travail et à T1 et T2. \newline
pour \(0 \leq j \leq 63\) (boucle)
\[T1 = h + \Sigma_1(e) + Ch(e, f, g) + K[j] + W[j]\]
\[T2 = \Sigma_0(a) + Maj(a, b, c)\]
\[h = g\]
\[g = f\]
\[f = e\]
\[e = d + T1\]
\[d = c\]
\[c = b\]
\[b = a\]
\[a = T1 + T2\] \newline

À la fin de cette boucle, et avec les valeurs de \(a, b, c, d, e, f, g\) et \(h\), on calcule les nouvelles valeurs de hachage intermédiaires :

\[hash[0] = a + hash[0]\]
\[hash[1] = b + hash[1]\]
\[hash[2] = c + hash[2]\]
\[hash[3] = d + hash[3]\]
\[hash[4] = e + hash[4]\]
\[hash[5] = f + hash[5]\]
\[hash[6] = g + hash[6]\]
\[hash[7] = h + hash[7]\]

\paragraph{Résultat : }
Une fois ces opérations effectuées pour tous les blocs du résultat du bourrage, le résultat du hachage est contenu dans \(hash\). \newline
Notons \(||\) l'opérateur de concaténation, le résultat est :
\[hash[0] || hash[1] || hash[2] || hash[3] || hash[4] || hash[5] || hash[6] || hash[7]\]

\paragraph{Références \\}
\url{http://www.iwar.org.uk/comsec/resources/cipher/sha256-384-512.pdf} \\
\url{https://helix.stormhub.org/papers/SHA-256.pdf} \\

\end{document}

