#include "ed25519.h"

#include "cryptopp/sha.h"
#include "cryptopp/hex.h"

// static members

const mpz_class nsSignature::Ed25519::_p("57896044618658097711785492504343953926634992332820282019728792003956564819949");
const mpz_class nsSignature::Ed25519::_d("37095705934669439343138083508754565189542113879843219016388785533085940283555");

const nsSignature::Ed25519::Point nsSignature::Ed25519::_B(Point::createEd25519Point(
        mpz_class("15112221349535400772501151409588531511454012693041857206046113283949847762202"),
        mpz_class("46316835694926478169428394003475163141307993866256225615783033603165251855960"),
        1,
        mpz_class("46827403850823179245072216630277197565144205554125654976674165829533817101731")));

const nsSignature::Ed25519::Point nsSignature::Ed25519::NEUTRAL_ELEMENT(Point::createEd25519Point(0, 1, 1, 0));
const mpz_class nsSignature::Ed25519::_L("7237005577332262213973186563042994240857116359379907606001950938285454250989");
const size_t nsSignature::Ed25519::SIGNATURE_SIZE = 64;


/*********************************************************************************/

nsSignature::Ed25519::Point nsSignature::Ed25519::decode(const std::vector<CryptoPP::byte> &vec) const
{
    if(vec.size() != SIGNATURE_SIZE / 2)
        throw std::invalid_argument("invalid size");

    // First, interpret the string as an integer in little-endian
    mpz_class y(0);
    for(size_t i(vec.size()); i-- > 0; )
        y = y * 256 + vec[i];

    // Bit 255 of this number is the least significant bit of the x-coordinate
    mpz_class x_0(y >> 255);

    // The y-coordinate is recovered simply by clearing this bit
    mpz_class mask(1);
    mask <<= 255;
    mask -= 1;
    y &= mask;

    if(y >= _p)
        throw std::invalid_argument("cannot be decoded");

    mpz_class u(y * y - 1);
    mpz_class v(_d * y * y + 1);
    mpz_class x2(u);
    mpz_class temp;
    mpz_invert(temp.get_mpz_t(), v.get_mpz_t(), _p.get_mpz_t());
    x2 *= temp;

    temp = (_p + 3) / 8;

    mpz_class x;
    mpz_powm(x.get_mpz_t(), x2.get_mpz_t(), temp.get_mpz_t(), _p.get_mpz_t());

    if(((v * x * x) + u) % _p == 0)
        x = (x * mpz_class("19681161376707505956807079304988542015446066515923890162744021073123829784752")) % _p;
    else if(((v * x * x) - u) % _p != 0)
        throw std::invalid_argument("cannot be decoded");


    if(x == 0 && x_0 == 1)
        throw std::invalid_argument("cannot be decoded");

    if(x_0 != x % 2)
        x = _p - x;


    return Point::createEd25519Point(x, y, 1, (x*y) % _p);
}

size_t nsSignature::Ed25519::getSignatureSize() const
{
    return SIGNATURE_SIZE;
}

std::vector<CryptoPP::byte> nsSignature::Ed25519::hash(const std::vector<CryptoPP::byte> &data, const std::string& context) const
{
    (void)context; // unused
    CryptoPP::SHA512 hash;
    std::vector<CryptoPP::byte> digest(CryptoPP::SHA512::DIGESTSIZE);

    hash.CalculateDigest(digest.data(), data.data(), data.size());
    return digest;
}

nsSignature::Ed25519::~Ed25519() {}

std::vector<CryptoPP::byte> nsSignature::Ed25519::sign(const std::string& m, const std::string& context /* std::string() */) const
{
    if(nullptr == getPrivateKey())
        throw std::runtime_error("private key must be set");

    if("" != context)
        throw std::invalid_argument("Context must be empty for Ed25519");

    std::vector<CryptoPP::byte> mHex(prepareMessage(m));

    nsKeys::InterpretedPrivateKey<nsKeys::Ed25519PublicKey> pk(getPrivateKey()->getAssociatedPublicKey());

    std::vector<CryptoPP::byte> concatPrefixAndM(pk.prefix.size() + mHex.size());
    std::copy(pk.prefix.begin(), pk.prefix.end(), concatPrefixAndM.begin());
    std::copy(mHex.begin(), mHex.end(), concatPrefixAndM.begin() + pk.prefix.size());

    CryptoPP::SHA512 hash;
    std::vector<CryptoPP::byte> digest(CryptoPP::SHA512::DIGESTSIZE);
    hash.CalculateDigest(digest.data(), concatPrefixAndM.data(), concatPrefixAndM.size());
    hash.Restart();

    mpz_class r(0);
    for(size_t i(digest.size()); i-- > 0; )
        r = r * 256 + digest[i];
    r = r % _L;

    Point B2(Point::getBasePoint());
    B2.pointMul(r);

    std::vector<CryptoPP::byte> encodedB2(encode(B2));
    std::vector<CryptoPP::byte> message = encodedB2;

    std::vector<CryptoPP::byte> encoded2(encode(pk.publicKey.getPoint()));

    message.insert(message.end(), encoded2.begin(), encoded2.end());
    message.insert(message.end(), mHex.begin(), mHex.end());

    hash.CalculateDigest(digest.data(), (CryptoPP::byte*) message.data(), message.size());
    hash.Restart();

    mpz_class numericDigest(0);
    for(size_t i(digest.size()); i-- > 0; )
        numericDigest = 256 * numericDigest + digest[i];
    numericDigest %= _L;
    mpz_class S(r + numericDigest * pk.s);
    S = S % _L;


    std::vector<CryptoPP::byte> result2(encode(S));
    encodedB2.insert(encodedB2.end(), result2.begin(), result2.end());
    return encodedB2;
}
