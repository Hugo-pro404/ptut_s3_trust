#include <iostream>
#include <list>

#include "cryptopp/sha.h"
#include "cracker.h"

using namespace std;

int main()
{


    std::string message = "hash"; // doit être non vide

    CryptoPP::SHA256 hashSHA256;
    std::string hash;
    hashSHA256.Update((const byte*)message.data(), message.size());
    hash.resize(hashSHA256.DigestSize());
    hashSHA256.Final((byte*)&hash[0]);
    hashSHA256.Restart();

    Password::Cracker cracker(hash);

    cout << "mot de passe à trouver : " << message << endl;

    std::cout << "debut" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    // debut chrono

    if(cracker.crack(4, Password::Cracker::ALPHA_NUM_SPECIAL))
        cout << cracker.getResult() << endl;
    else
        cout << "le mot de passe n'a pas été trouvé" << endl;

    // fin chrono
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

    std::cout << "Message : \""<< cracker.getResult() <<"\""<< std::endl;
    std::cout << "Nb tests : " << cracker.getNbTests() << std::endl;
    std::cout << "temps d'execution : " << duration.count() << " millisecondes" << std::endl;

    return 0;
}
