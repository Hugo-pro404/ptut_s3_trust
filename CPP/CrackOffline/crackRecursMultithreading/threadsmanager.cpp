#include <iostream>
#include "threadsmanager.h"

using namespace std;

Password::ThreadsManager::ManagedThread::ManagedThread(unique_ptr<thread> thread)
    : m_thread(move(thread)), m_terminated(false)
{}

Password::ThreadsManager::ThreadsManager()
    : m_answerFound(false)
{
    if(thread::hardware_concurrency() <= 1)
        throw runtime_error("non supporté");
}

bool Password::ThreadsManager::hasAnswerBeenFound()
{
    return m_answerFound;
}

void Password::ThreadsManager::setAnswerFoundFlag()
{
    m_answerFound.store(true);
}

void Password::ThreadsManager::markAsTerminated(const thread::id& id)
{
    lock_guard<mutex> lockGuard(m_threadsMutex);
    vector<unique_ptr<ManagedThread>>::iterator it(m_threads.begin());
    while(it != m_threads.end() && (*it)->m_thread->get_id() != id)
        ++it;

    if(it == m_threads.end())
        return;

    (*it)->m_terminated = true;
}

void Password::ThreadsManager::waitForAvailableThread()
{
    cout << "Attente de libération d'un thread" << endl;
    cout << m_threads.size() << " / " << thread::hardware_concurrency() - 1 << endl;
    cout << " ----- " << endl;
    while(!aThreadIsTerminated())
        this_thread::yield();

    lock_guard<mutex> lockGuardThreads(m_threadsMutex);

    vector<unique_ptr<ManagedThread>>::iterator it(m_threads.begin());
    while(it != m_threads.end() && !((*it)->m_terminated))
        ++it;
    (*it)->m_thread->join();

    m_threads.erase(it);
}

bool Password::ThreadsManager::aThreadIsAvailable()
{
    return ((thread::hardware_concurrency() - m_threads.size() - 1) > 0);
}

bool Password::ThreadsManager::addThread(unique_ptr<thread> t)
{
    lock_guard<mutex> lockGuardThreads(m_threadsMutex);

    vector<unique_ptr<ManagedThread>>::iterator it(m_threads.begin());
    while(it != m_threads.end() && (*it)->m_thread != t)
        ++it;

    if(it == m_threads.end() && m_threads.size() < thread::hardware_concurrency())
    {
        m_threads.push_back(make_unique<ManagedThread>(move(t)));
        return true;
    }

    return false;
}

void Password::ThreadsManager::waitForEndOfAllThreads()
{
    for(const unique_ptr<ManagedThread>& mt : m_threads)
        mt->m_thread->join();
}

bool Password::ThreadsManager::aThreadIsTerminated()
{
    for(vector<unique_ptr<ManagedThread>>::iterator it(m_threads.begin()); it != m_threads.end(); ++it)
        if((*it)->m_terminated)
            return true;
    return false;
}
