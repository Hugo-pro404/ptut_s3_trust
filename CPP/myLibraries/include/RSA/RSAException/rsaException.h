#ifndef SIGNATUREEXCEPTION
#define SIGNATUREEXCEPTION

#include <exception>
#include <string>
#include <ostream>

#include "rsaExceptionCode.h"

namespace nsCrypto
{
    class RSAException : public std::exception
    {
        std::string m_str;
        RSAExceptionCode m_c;

    public:
        RSAException(const std::string& str, const RSAExceptionCode& c);
        virtual ~RSAException() override;
        virtual const char * what() const noexcept override;
        void display(std::ostream &os) const;
    };
}


#endif // SIGNATUREEXCEPTION
