#include "rsaprivatekey.h"
#include "numericutils.h"
#include "RSAException/rsaException.h"
#include "RSAException/rsaExceptionCode.h"

using namespace std;

/*explicit*/ nsCrypto::RSAPrivateKey::RSAPrivateKey(unsigned long bits)
{
    mpz_class limit;
    mpz_ui_pow_ui(limit.get_mpz_t(), 2, bits - 1);
    while(m_p * m_q < limit)
    {
        m_p = nsNumericUtils::randomPrime_n_bits(bits/2);
        m_q = nsNumericUtils::randomPrime_n_bits(bits/2);
        while(m_p == m_q)
            m_q = nsNumericUtils::randomPrime_n_bits(bits/2);
    }

    setN(m_p * m_q);
    setExponent(nsNumericUtils::randomCoprime((m_p - 1)*(m_q - 1)));
} // RSAPrivateKey

nsCrypto::RSAPrivateKey::RSAPrivateKey(const mpz_class& p, const mpz_class& q)
    : m_p(p), m_q(q)
{
    setExponent(nsNumericUtils::randomCoprime((m_p - 1)*(m_q - 1)));
    setN(m_p * m_q);
    // il n'est pas nécessaire de trouver l'inverse car nous ne générons que la clé privée
} // RSAPrivateKey

/* static */ nsCrypto::RSAPrivateKey nsCrypto::RSAPrivateKey::createFromNSize(unsigned long bits)
{
    if(bits % 2 != 0)
        throw nsCrypto::RSAException("key length must be even", nsCrypto::RSAExceptionCode::InvalidKeyLength);
    if(bits < 6)
        throw nsCrypto::RSAException("key length is too short", nsCrypto::RSAExceptionCode::InvalidKeyLength);

    return RSAPrivateKey(bits);
}

/* static */ nsCrypto::RSAPrivateKey nsCrypto::RSAPrivateKey::createFromTwoPrimes(const mpz_class &p, const mpz_class &q)
{
    if(p == q)
        throw nsCrypto::RSAException("p and q must not be equal", nsCrypto::RSAExceptionCode::PandQAreEqual);

    if(!nsNumericUtils::isPrime(p) || !nsNumericUtils::isPrime(q))
        throw nsCrypto::RSAException("p and q must be prime", nsCrypto::RSAExceptionCode::PrimalityError);

    return RSAPrivateKey(p, q);
}

nsCrypto::RSAPublicKey nsCrypto::RSAPrivateKey::getAssociatedPublicKey() const
{
    //may throw
    //invalid argument
    return RSAPublicKey(nsNumericUtils::modularInverse(getExponent(), (m_p - 1) * (m_q - 1)), getN());
} // getAssociatedPublicKey

const mpz_class& nsCrypto::RSAPrivateKey::getP() const { return m_p; }
const mpz_class& nsCrypto::RSAPrivateKey::getQ() const { return m_q; }

bool nsCrypto::RSAPrivateKey::isPublicKeyValid(const shared_ptr<nsCrypto::RSAPublicKey>& pk) const
{
    mpz_class phi((m_p - 1) * (m_q - 1));
    if((pk->getExponent() * getExponent()) % phi != 1)
        return false;
    return true;
} // isPublicKeyValid

void nsCrypto::RSAPrivateKey::setP(const mpz_class& p)
{
    if(!nsNumericUtils::isPrime(p))
        throw nsCrypto::RSAException("P must be prime", nsCrypto::RSAExceptionCode::PrimalityError);
    if(p == m_q)
        throw nsCrypto::RSAException("p and q must not be equal", nsCrypto::RSAExceptionCode::PandQAreEqual);

    m_p = p;
    setN(m_p * m_q);
}

void nsCrypto::RSAPrivateKey::setQ(const mpz_class& q)
{
    if(!nsNumericUtils::isPrime(q))
        throw nsCrypto::RSAException("Q must be prime", nsCrypto::RSAExceptionCode::PrimalityError);
    if(q == m_p)
        throw nsCrypto::RSAException("p and q must not be equal", nsCrypto::RSAExceptionCode::PandQAreEqual);

    m_q = q;
    setN(m_p * m_q);
}
