#include <sstream>
#include <iomanip>
#include "sha1.h"

using namespace std;

const array<uint32_t, 4> nsHash::SHA1::K{0x5a827999,
                                         0x6ed9eba1,
                                         0x8f1bbcdc,
                                         0xca62c1d6};

const array<uint32_t, 5> nsHash::SHA1::registersInit{0x67452301,
                                                     0xefcdab89,
                                                     0x98badcfe,
                                                     0x10325476,
                                                     0xc3d2e1f0};

nsHash::SHA1::SHA1(const std::string & input /* = std::string() */)
    : HashFunction(input), m_hash(registersInit)
{}

uint32_t nsHash::SHA1::choice(uint32_t x, uint32_t y, uint32_t z) const
{
    return ((x & y) | ((~x) & z));
}

uint32_t nsHash::SHA1::majority(uint32_t x, uint32_t y, uint32_t z) const
{
    return ((x & y) | (x & z) | (y & z));
}

uint32_t nsHash::SHA1::parity(uint32_t x, uint32_t y, uint32_t z) const
{
    return (x ^ y ^ z);
}

uint32_t nsHash::SHA1::Kval(uint32_t i) const
{
    if(i >= 80)
        return 0;
    return K[i / 20];
}

array<uint32_t, 16> nsHash::SHA1::toTab(const std::array<uint8_t, blocksSize / 8> &tab)
{
    array<uint32_t, 16> res;
    for(unsigned i(0); 4*i < tab.size(); ++i)
    {
        res[i] = 0;
        for(unsigned j(0); j < 4 && (4*i) + j < tab.size(); ++j)
            res[i] = res[i] | (uint32_t(tab[(4*i) + j]) << (8*(sizeof(uint32_t) - (j + 1))));
    }

    return res;
}

//ajouter 100000...
void nsHash::SHA1::addPadding(array<uint8_t, 16 * 4>& tab, size_t pos, uint64_t l, bool add1)
{
    if(add1)
    {
        tab[pos] = uint8_t(1) << (sizeof(uint8_t)*8 - 1); // 10000000...
        ++pos;
    }
    while((pos * (sizeof(uint8_t)*8) + 64) % blocksSize != 0)
    {
        tab[pos] = 0;
        ++pos;
    }

    for(unsigned i(0); i < 8; ++i) // ajout de la valeur de l à la fin sur 64 bits
    {
        uint64_t mask(255);
        mask <<= 64 - (8 * (i + 1));
        tab[pos + i] = (l & mask) >> (64 - (8 * (i + 1)));
    }
}

const array<uint32_t, 5>& nsHash::SHA1::getResult() const
{
    return m_hash;
}

std::array<uint8_t, 20> nsHash::SHA1::getResultBytes() const
{
    array<uint8_t, 20> result;
    for(unsigned i(0); i < result.size(); ++i)
        result[i] = uint8_t((m_hash[i / 4] & (uint32_t(255) << ((3 - i)*8))) >> ((3 - i)*8));
    return result;
}

std::string nsHash::SHA1::getResultAsString() const
{
    ostringstream ostr;
    ostr.fill('0');
    for(unsigned i(0); i < m_hash.size(); ++i)
        ostr << hex << setw(8) << m_hash[i];
    return ostr.str();
}


void nsHash::SHA1::computeString()
{
    resetHash();
    istringstream istr(m_inputData);
    compute(istr);
}

void nsHash::SHA1::computeFile()
{
    resetHash();
    ifstream file;
    file.exceptions(ios_base::failbit);
    file.open(m_inputData, ios_base::binary); // throw std::ios_base::failure si le fichier n'est pas ouvert
    file.exceptions(ios_base::goodbit); // désactiver les exceptions
    compute(file);
}

void nsHash::SHA1::compute(istream &stream)
{
    array<uint32_t, 16> tab;
    array<uint8_t, 16*4> tempTab;
    bool fin(false);
    bool add1(true);
    while(!fin)
    {
        unsigned i(0);
        char in;
        while(i < tempTab.size() && stream.get(in))
        {
            tempTab[i] = in;
            ++i;
        }

        if(/*!stream && */ i + 8 < tempTab.size())
        {
            stream.clear();
            addPadding(tempTab, i, stream.tellg()*8, add1);
            fin = true;
        }
        else if(i < tempTab.size())
        {
            tempTab[i] = uint8_t(1) << (sizeof(uint8_t)*8 - 1); // début du padding pour comptéter le bloc
            ++i;
            while(i < tempTab.size())
            {
                tempTab[i] = 0;
                ++i;
            }
            add1 = false;
        }

        tab = toTab(tempTab);

        // traitement principal
        array<uint32_t, 80> W;
        copy(tab.begin(), tab.end(), W.begin());
        for(unsigned j(16); j < W.size(); ++j)
            W[j] = RotateLeft(W[j - 3] ^ W[j - 8] ^ W[j - 14] ^ W[j - 16], 1);

        uint32_t a(m_hash[0]),
                b(m_hash[1]),
                c(m_hash[2]),
                d(m_hash[3]),
                e(m_hash[4]);

        for(unsigned j(0); j < 80; ++j)
        {
            uint32_t temp(RotateLeft(a, 5) + ft(j, b, c, d) + e + Kval(j) + W[j]);
            e = d;
            d = c;
            c = RotateLeft(b, 30);
            b = a;
            a = temp;
        }
        m_hash[0] = a + m_hash[0];
        m_hash[1] = b + m_hash[1];
        m_hash[2] = c + m_hash[2];
        m_hash[3] = d + m_hash[3];
        m_hash[4] = e + m_hash[4];
    }

    m_valid = true;
}

uint32_t nsHash::SHA1::RotateLeft(uint32_t val, unsigned short n)
{
    n %= sizeof(val)*8;
    uint32_t mask((uint32_t(1) << n) - 1);
    mask = mask << ((sizeof(val)*8) - n);
    return (val << n) | ((val & mask) >> ((sizeof(val)*8) - n));
}

uint32_t nsHash::SHA1::ft(uint32_t t, uint32_t b, uint32_t c, uint32_t d) const
{
    t = t / 20;

    if(t == 0)
        return choice(b, c, d);
    if(t == 1 || t == 3)
        return parity(b, c, d);
    if(t == 2)
        return majority(b, c, d);

    return 0;
}

void nsHash::SHA1::resetHash()
{
    m_valid = false;
    m_hash = registersInit;
}
